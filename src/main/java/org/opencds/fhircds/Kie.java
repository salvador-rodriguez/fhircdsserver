package org.opencds.fhircds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.builder.KieScanner;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.socraticgrid.fhir.generated.AdapterFactory;
import org.socraticgrid.fhir.generated.QICoreConditionAdapter;
import org.socraticgrid.fhir.generated.QICoreObservationAdapter;
import org.socraticgrid.fhir.generated.QICorePatientAdapter;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.Condition;

import ca.uhn.fhir.parser.IParser;

public class Kie {

	public Bundle Evaluation(String groupId, String artifactId,
			String versionId, String stringBundle) {

		FhirContext ctx = new FhirContext();
		IParser parser = ctx.newXmlParser();
		Bundle bundle = parser.parseBundle(stringBundle);
		AdapterFactory factory = new AdapterFactory();
		factory.addProfileToAdapterMap(
				"http://hl7.org/fhir/StructureDefinition/patient-qicore-qicore-patient",
				QICorePatientAdapter.class);
		factory.addProfileToAdapterMap(
				"http://hl7.org/fhir/StructureDefinition/condition-qicore-qicore-condition",
				QICoreConditionAdapter.class);
		Map<String, List<?>> resourcesMap = factory.adapt(bundle);

		List<?> globalList = new ArrayList<>();

		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.newKieContainer(ks.newReleaseId(groupId,
				artifactId, versionId));
		KieScanner kScan = ks.newKieScanner(kContainer);
		kScan.scanNow();
		KieSession kSession = kContainer.newKieSession();
		kSession.setGlobal("globalList", globalList);

		// insert facts
		for (List<?> resources : resourcesMap.values()) {
			for (Object resource : resources) {
				if (resource != null) {
					kSession.insert(resource);
				}
			}
		}

		kSession.fireAllRules();
		kSession.dispose();
		
		return BuildOutputBundle(globalList, ctx);
	}

	public Bundle Evaluation(String stringBundle) {

		FhirContext ctx = new FhirContext();
		IParser parser = ctx.newXmlParser();
		Bundle bundle = parser.parseBundle(stringBundle);
		AdapterFactory factory = new AdapterFactory();
		factory.addProfileToAdapterMap(
				"http://hl7.org/fhir/StructureDefinition/patient-qicore-qicore-patient",
				QICorePatientAdapter.class);
		factory.addProfileToAdapterMap(
				"http://hl7.org/fhir/StructureDefinition/condition-qicore-qicore-condition",
				QICoreConditionAdapter.class);
		Map<String, List<?>> resourcesMap = factory.adapt(bundle);

		List<?> globalList = new ArrayList<>();
		// Map<String, List<?>> map = new HashMap<String, List<?>>();
		// map.put("list", globalList);

		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		kSession.setGlobal("globalList", globalList);

		for (List<?> resources : resourcesMap.values()) {
			for (Object resource : resources) {
				if (resource != null) {
					kSession.insert(resource);
				}
			}
		}

		kSession.fireAllRules();
		kSession.dispose();

		return BuildOutputBundle(globalList, ctx);

	}

	private Bundle BuildOutputBundle(List<?> list, FhirContext ctx) {

		Bundle bundle = new Bundle();
		for (Object obj : list) {
			if (obj instanceof QICorePatientAdapter) {
				QICorePatientAdapter patientAdapter = (QICorePatientAdapter) obj;
				Patient patient = patientAdapter.getAdaptee();
				bundle.addResource(patient, ctx, "http://foo");
			}
			if (obj instanceof QICoreConditionAdapter) {
				QICoreConditionAdapter conditionAdapter = (QICoreConditionAdapter) obj;
				Condition condition = conditionAdapter.getAdaptee();
				bundle.addResource(condition, ctx, "http://foo");
			}
			if (obj instanceof QICoreObservationAdapter) {
				QICoreObservationAdapter observationAdapter = (QICoreObservationAdapter) obj;
				Observation observation = observationAdapter.getAdaptee();
				bundle.addResource(observation, ctx, "http://foo");
			}
		}
		return bundle;
	}

}
