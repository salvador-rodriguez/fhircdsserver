package org.opencds.fhircds;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingType;

import org.apache.commons.codec.binary.Base64;
import org.omg.spec.cdss._201105.dss.DataRequirementItemData;
import org.omg.spec.cdss._201105.dss.EntityIdentifier;
import org.omg.spec.cdss._201105.dss.EvaluationRequest;
import org.omg.spec.cdss._201105.dss.EvaluationResponse;
import org.omg.spec.cdss._201105.dss.FinalKMEvaluationResponse;
import org.omg.spec.cdss._201105.dss.InteractionIdentifier;
import org.omg.spec.cdss._201105.dss.ItemIdentifier;
import org.omg.spec.cdss._201105.dss.IterativeEvaluationRequest;
import org.omg.spec.cdss._201105.dss.IterativeEvaluationResponse;
import org.omg.spec.cdss._201105.dss.KMEvaluationResultData;
import org.omg.spec.cdss._201105.dss.SemanticPayload;
import org.omg.spec.cdss._201105.dsswsdl.DSSRuntimeExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.Evaluation;
import org.omg.spec.cdss._201105.dsswsdl.EvaluationExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.InvalidDriDataFormatExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.InvalidTimeZoneOffsetExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.RequiredDataNotProvidedExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.UnrecognizedLanguageExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.UnrecognizedScopedEntityExceptionFault;
import org.omg.spec.cdss._201105.dsswsdl.UnsupportedLanguageExceptionFault;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.Bundle;

@javax.jws.WebService(
        serviceName = "DecisionSupportService",
        portName = "evaluate",
        targetNamespace = "http://www.omg.org/spec/CDSS/201105/dssWsdl",
        wsdlLocation = "evaluate.wsdl",
        endpointInterface = "org.omg.spec.cdss._201105.dsswsdl.Evaluation")
		/*@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)*/
public class EvaluateImpl implements Evaluation {

	private static final Logger LOG = Logger.getLogger(EvaluateImpl.class
			.getName());

    public org.omg.spec.cdss._201105.dss.EvaluationResponse evaluateAtSpecifiedTime(org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,javax.xml.datatype.XMLGregorianCalendar specifiedTime,org.omg.spec.cdss._201105.dss.EvaluationRequest evaluationRequest) throws InvalidTimeZoneOffsetExceptionFault , EvaluationExceptionFault , InvalidDriDataFormatExceptionFault , UnrecognizedLanguageExceptionFault , UnrecognizedScopedEntityExceptionFault , DSSRuntimeExceptionFault , UnsupportedLanguageExceptionFault , RequiredDataNotProvidedExceptionFault    {
		LOG.info("Executing operation evaluateAtSpecifiedTime");

		try {

			List<DataRequirementItemData> dataRequirementItemDataList = evaluationRequest
					.getDataRequirementItemData();

			DataRequirementItemData requitementItemData = dataRequirementItemDataList
					.get(0);

			ItemIdentifier driId = requitementItemData.getDriId();

			// get driId
			String itemId = driId.getItemId();
			//System.out.println(itemId);
			EntityIdentifier containingEntity = driId.getContainingEntityId();

			/**
			 * get Kie project details, for example: scopingEntityId =
			 * "group_id" businessId = "artifact_id" version =
			 * "artifact_version"
			 * 
			 */

			String group_id = containingEntity.getScopingEntityId();
			String artifact_id = containingEntity.getBusinessId();
			String artifact_version = containingEntity.getVersion();

			/**
			 * get vMR for now easycds will support only one payload even though
			 * the CDS Service spec allows using several payloads
			 * 
			 */

			// get data
			SemanticPayload data = requitementItemData.getData();

			// get data informationModelSSId
			EntityIdentifier informationModelSSId = data
					.getInformationModelSSId();

			// get base64EncodedPayload
			List<?> base64EncodedPayloadList = data.getBase64EncodedPayload();

			String base64Payload = new String(
					Base64.decodeBase64((String) base64EncodedPayloadList
							.get(0)));
			//System.out.println(base64Payload);

			// CDSInput cdsInput = UnMarshalVmr(decodedPayload);

			// Kie Evaluation
			Kie kie = new Kie();
			Bundle bundle = kie.Evaluation(group_id, artifact_id,
					artifact_version, base64Payload);
			FhirContext context = FhirContext.forDstu2();
			String stringBundle = context.newXmlParser().setPrettyPrint(true)
					.encodeBundleToString(bundle);

			// Kie evaluation results
			// String strCdsOutput = marshallVmr(cdsOutput);
			// System.out.println(strCdsOutput);

			// set CDS service response
			org.omg.spec.cdss._201105.dss.ObjectFactory dssFactory = new org.omg.spec.cdss._201105.dss.ObjectFactory();
			EvaluationResponse response = dssFactory.createEvaluationResponse();

			List<FinalKMEvaluationResponse> finalKMEvaluationResponseList = response
					.getFinalKMEvaluationResponse();
			FinalKMEvaluationResponse finalKMEvaluationResponse = dssFactory
					.createFinalKMEvaluationResponse();

			// set KmId
			EntityIdentifier kmId = dssFactory.createEntityIdentifier();
			kmId.setScopingEntityId(group_id);
			kmId.setBusinessId(artifact_version);
			kmId.setVersion(artifact_version);
			finalKMEvaluationResponse.setKmId(kmId);

			// set kmEvalutionResultData
			List<KMEvaluationResultData> kmEvaluationResultDataList = finalKMEvaluationResponse
					.getKmEvaluationResultData();
			KMEvaluationResultData kmEvaluationResultData = dssFactory
					.createKMEvaluationResultData();

			// set evaluationResultId
			ItemIdentifier itemIdentifier = dssFactory.createItemIdentifier();
			itemIdentifier.setItemId(itemId + ".EvaluationResult");
			kmEvaluationResultData.setEvaluationResultId(itemIdentifier);

			// set data
			SemanticPayload semanticPayload = dssFactory.createSemanticPayload();

			// set informationModelISSSId
			EntityIdentifier easyCDSModelSSID = dssFactory
					.createEntityIdentifier();
			
			easyCDSModelSSID.setScopingEntityId("org.opencds.fhir");
			easyCDSModelSSID.setBusinessId("FHIR");
			easyCDSModelSSID.setVersion("1.0");
			semanticPayload.setInformationModelSSId(easyCDSModelSSID);

			// set base64EncodedPayload
			semanticPayload.getBase64EncodedPayload().add(new String(Base64.encodeBase64(stringBundle.getBytes())));
			// base64EncodedList.add(stringBundle);
			//base64EncodedList.add(stringBundle.getBytes());

			kmEvaluationResultData.setData(semanticPayload);
			kmEvaluationResultDataList.add(kmEvaluationResultData);
			finalKMEvaluationResponseList.add(finalKMEvaluationResponse);

			return response;
		} catch (java.lang.Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		// throw new
		// InvalidTimeZoneOffsetExceptionFault("InvalidTimeZoneOffsetExceptionFault...");
		// throw new EvaluationExceptionFault("EvaluationExceptionFault...");
		// throw new
		// InvalidDriDataFormatExceptionFault("InvalidDriDataFormatExceptionFault...");
		// throw new
		// UnrecognizedLanguageExceptionFault("UnrecognizedLanguageExceptionFault...");
		// throw new
		// UnrecognizedScopedEntityExceptionFault("UnrecognizedScopedEntityExceptionFault...");
		// throw new DSSRuntimeExceptionFault("DSSRuntimeExceptionFault...");
		// throw new
		// UnsupportedLanguageExceptionFault("UnsupportedLanguageExceptionFault...");
		// throw new
		// RequiredDataNotProvidedExceptionFault("RequiredDataNotProvidedExceptionFault...");
	}

	@Override
	public EvaluationResponse evaluate(InteractionIdentifier interactionId,
			EvaluationRequest evaluationRequest)
			throws InvalidTimeZoneOffsetExceptionFault,
			EvaluationExceptionFault, InvalidDriDataFormatExceptionFault,
			UnrecognizedLanguageExceptionFault,
			UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault,
			UnsupportedLanguageExceptionFault,
			RequiredDataNotProvidedExceptionFault {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IterativeEvaluationResponse evaluateIterativelyAtSpecifiedTime(
			InteractionIdentifier interactionId,
			XMLGregorianCalendar specifiedTime,
			IterativeEvaluationRequest iterativeEvaluationRequest)
			throws InvalidTimeZoneOffsetExceptionFault,
			EvaluationExceptionFault, InvalidDriDataFormatExceptionFault,
			UnrecognizedLanguageExceptionFault,
			UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault,
			UnsupportedLanguageExceptionFault,
			RequiredDataNotProvidedExceptionFault {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IterativeEvaluationResponse evaluateIteratively(
			InteractionIdentifier interactionId,
			IterativeEvaluationRequest iterativeEvaluationRequest)
			throws InvalidTimeZoneOffsetExceptionFault,
			EvaluationExceptionFault, InvalidDriDataFormatExceptionFault,
			UnrecognizedLanguageExceptionFault,
			UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault,
			UnsupportedLanguageExceptionFault,
			RequiredDataNotProvidedExceptionFault {
		// TODO Auto-generated method stub
		return null;
	}


}