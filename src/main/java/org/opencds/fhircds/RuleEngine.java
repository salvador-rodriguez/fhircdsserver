package org.opencds.fhircds;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socraticgrid.fhir.generated.AdapterFactory;
import org.socraticgrid.fhir.generated.QICoreConditionAdapter;
import org.socraticgrid.fhir.generated.QICorePatientAdapter;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.model.api.ResourceMetadataKeyEnum;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.parser.IParser;

public class RuleEngine {

	static public final String GENERATED_PACKAGE_PREFIX = "org.socraticgrid.fhir.generated.";
	static public final String HAPI_FHIR_RESOURCE_PREFIX = "ca.uhn.fhir.model.dstu2.resource.";
	static public final Logger LOGGER = LoggerFactory
			.getLogger(AdapterFactory.class);
	private Map<String, Class<?>> profileToAdapterClassMap;

	public RuleEngine() {
		profileToAdapterClassMap = new HashMap<>();
	}

	void insertFacts(String input) {

		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieSession kSession = kContainer.newKieSession("ksession-rules");
		StringReader reader = new StringReader(input);

		FhirContext ctx = new FhirContext();
		IParser parser = ctx.newXmlParser();
		Bundle bundle = parser.parseBundle(input);

		addProfileToAdapterMap("http://hl7.org/fhir/StructureDefinition/patient-qicore-qicore-patient", QICorePatientAdapter.class);
		addProfileToAdapterMap("http://hl7.org/fhir/StructureDefinition/condition-qicore-qicore-condition", QICoreConditionAdapter.class);
		Map<String, List<?>> resourcesMap = adapt(bundle);
				
		for (List<?> resources: resourcesMap.values())
		{
			for (Object resource : resources)
			{		
				if (resource != null)
				{
					kSession.insert(resource);
				}
			}			
		}
		
		int result = kSession.fireAllRules();

	}

	public void addProfileToAdapterMap(String profile, Class<?> adapter) {
		profileToAdapterClassMap.put(profile, adapter);
	}

	public Map<String, List<?>> adapt(ca.uhn.fhir.model.api.Bundle bundle) {
		Map<String, List<?>> adapteeMap = new java.util.HashMap<>();
		for (ca.uhn.fhir.model.api.BundleEntry entry : bundle.getEntries()) {
			Object adapter = adapt(entry.getResource());
			if (adapter != null) {
				List list = adapteeMap.get(entry.getResource().getClass().getCanonicalName());
				if (list == null) {
					list = new java.util.ArrayList();
					adapteeMap.put(entry.getResource().getClass()
					.getCanonicalName(), list);
				}
				list.add(adapter);
			}
		}
		return adapteeMap;
	}

	public Object adapt(ca.uhn.fhir.model.api.IResource resource)
	{
		Object adapter = null;
		Class<?> adapterClass = null;
		List<IdDt> profiles = ResourceMetadataKeyEnum.PROFILES.get(resource);
		if (profiles == null || profiles.size() <= 0) {
			throw new RuntimeException("A resource must specify a profile tag");
		} else {
			String profile = profiles.get(0).getValueAsString();
			adapterClass = profileToAdapterClassMap.get(profile);
		}
		String resourceName = resource.getResourceName();
		try {
			adapter = adapterClass.newInstance();
			Class[] args = new Class[1];
			args[0] = Class.forName(HAPI_FHIR_RESOURCE_PREFIX + resourceName);
			java.lang.reflect.Method method = adapterClass.getDeclaredMethod(
					"setAdaptee", args);
			Object[] resourceArray = new Object[1];
			resourceArray[0] = resource;
			method.invoke(adapter, resourceArray);
		}

		catch (Exception e) {
			LOGGER.error("Unable to adapt " + resourceName, e);
		}
		return adapter;
	}
}
