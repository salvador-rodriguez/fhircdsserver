
package org.omg.spec.cdss._201105.dsswsdl;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.4.6
 * 2015-05-14T01:22:45.692+01:00
 * Generated source version: 2.4.6
 */

@WebFault(name = "UnrecognizedScopedEntityException", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss")
public class UnrecognizedScopedEntityExceptionFault extends Exception {
    
    private org.omg.spec.cdss._201105.dss.UnrecognizedScopedEntityException unrecognizedScopedEntityException;

    public UnrecognizedScopedEntityExceptionFault() {
        super();
    }
    
    public UnrecognizedScopedEntityExceptionFault(String message) {
        super(message);
    }
    
    public UnrecognizedScopedEntityExceptionFault(String message, Throwable cause) {
        super(message, cause);
    }

    public UnrecognizedScopedEntityExceptionFault(String message, org.omg.spec.cdss._201105.dss.UnrecognizedScopedEntityException unrecognizedScopedEntityException) {
        super(message);
        this.unrecognizedScopedEntityException = unrecognizedScopedEntityException;
    }

    public UnrecognizedScopedEntityExceptionFault(String message, org.omg.spec.cdss._201105.dss.UnrecognizedScopedEntityException unrecognizedScopedEntityException, Throwable cause) {
        super(message, cause);
        this.unrecognizedScopedEntityException = unrecognizedScopedEntityException;
    }

    public org.omg.spec.cdss._201105.dss.UnrecognizedScopedEntityException getFaultInfo() {
        return this.unrecognizedScopedEntityException;
    }
}
