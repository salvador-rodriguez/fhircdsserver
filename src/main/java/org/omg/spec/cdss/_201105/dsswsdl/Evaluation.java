package org.omg.spec.cdss._201105.dsswsdl;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * The Evaluation Interface enables data evaluation using knowledge modules.
 *
 * This class was generated by Apache CXF 2.4.6
 * 2015-05-14T01:22:45.841+01:00
 * Generated source version: 2.4.6
 * 
 */
@WebService(targetNamespace = "http://www.omg.org/spec/CDSS/201105/dssWsdl", name = "Evaluation")
@XmlSeeAlso({org.omg.spec.cdss._201105.dss.ObjectFactory.class})
public interface Evaluation {

    /**
     * Throws one of the exceptions if an exception condition is present.  If none of the exception conditions are present, evaluates in a non-iterative fashion one or more knowledge modules using the data provided as an EvaluationRequest object and returns the result(s) of the evaluation as an EvaluationResponse object. 
     * Conducts evaluation as if it was currently the specified date and time.
     * All time-stamped data are considered to have the time zone offset specified by the client, unless otherwise noted.
     * The provision of excessive data (i.e., unrequired DataRequirementItemData) shall be ignored without leading to an exception.  However, a warning may be provided.
     */
    @WebResult(name = "evaluationResponse", targetNamespace = "")
    @RequestWrapper(localName = "evaluateAtSpecifiedTime", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateAtSpecifiedTime")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluateAtSpecifiedTime")
    @ResponseWrapper(localName = "evaluateAtSpecifiedTimeResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateAtSpecifiedTimeResponse")
    public org.omg.spec.cdss._201105.dss.EvaluationResponse evaluateAtSpecifiedTime(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "specifiedTime", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar specifiedTime,
        @WebParam(name = "evaluationRequest", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.EvaluationRequest evaluationRequest
    ) throws EvaluationExceptionFault, InvalidDriDataFormatExceptionFault, UnrecognizedScopedEntityExceptionFault, UnrecognizedLanguageExceptionFault, InvalidTimeZoneOffsetExceptionFault, DSSRuntimeExceptionFault, UnsupportedLanguageExceptionFault, RequiredDataNotProvidedExceptionFault;

    /**
     * Throws one of the exceptions if an exception condition is present.  If none of the exception conditions are present, evaluates in a non-iterative fashion one or more knowledge modules using the data provided as an EvaluationRequest object and returns the result(s) of the evaluation as an EvaluationResponse object. 
     * All time-stamped data are considered to have the time zone offset specified by the client, unless otherwise noted.
     * The provision of excessive data (i.e., unrequired DataRequirementItemData) shall be ignored without leading to an exception.  However, a warning may be provided.
     */
    @WebResult(name = "evaluationResponse", targetNamespace = "")
    @RequestWrapper(localName = "evaluate", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.Evaluate")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluate")
    @ResponseWrapper(localName = "evaluateResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateResponse")
    public org.omg.spec.cdss._201105.dss.EvaluationResponse evaluate(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "evaluationRequest", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.EvaluationRequest evaluationRequest
    ) throws EvaluationExceptionFault, InvalidDriDataFormatExceptionFault, UnrecognizedScopedEntityExceptionFault, UnrecognizedLanguageExceptionFault, InvalidTimeZoneOffsetExceptionFault, DSSRuntimeExceptionFault, UnsupportedLanguageExceptionFault, RequiredDataNotProvidedExceptionFault;

    /**
     * Throws one of the exceptions if an exception condition is present.  If none of the exception conditions are present, evaluates the data provided by the client using one or more knowledge modules and returns the result(s) of the evaluation.  Conducts evaluation iteratively, returning intermediate state data and specification of additional required data if final conclusions cannot be initially reached.
     * Conducts evaluation as if it was currently the specified date and time.
     * All time-stamped data are considered to have the time zone offset specified by the client, unless otherwise noted.
     * The provision of excessive data (i.e., unrequired DataRequirementItemData) shall be ignored without leading to an exception.  However, a warning may be provided.
     */
    @WebResult(name = "iterativeEvaluationResponse", targetNamespace = "")
    @RequestWrapper(localName = "evaluateIterativelyAtSpecifiedTime", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateIterativelyAtSpecifiedTime")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluateIterativelyAtSpecifiedTime")
    @ResponseWrapper(localName = "evaluateIterativelyAtSpecifiedTimeResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateIterativelyAtSpecifiedTimeResponse")
    public org.omg.spec.cdss._201105.dss.IterativeEvaluationResponse evaluateIterativelyAtSpecifiedTime(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "specifiedTime", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar specifiedTime,
        @WebParam(name = "iterativeEvaluationRequest", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.IterativeEvaluationRequest iterativeEvaluationRequest
    ) throws EvaluationExceptionFault, InvalidDriDataFormatExceptionFault, UnrecognizedScopedEntityExceptionFault, UnrecognizedLanguageExceptionFault, InvalidTimeZoneOffsetExceptionFault, DSSRuntimeExceptionFault, UnsupportedLanguageExceptionFault, RequiredDataNotProvidedExceptionFault;

    /**
     * Throws one of the exceptions if an exception condition is present.  If none of the exception conditions are present, evaluates the data provided by the client using one or more knowledge modules and returns the result(s) of the evaluation.  Conducts evaluation iteratively, returning intermediate state data and specification of additional required data if final conclusions cannot be initially reached.
     * All time-stamped data are considered to have the time zone offset specified by the client, unless otherwise noted.
     * The provision of excessive data (i.e., unrequired DataRequirementItemData) shall be ignored without leading to an exception.  However, a warning may be provided.
     */
    @WebResult(name = "iterativeEvaluationResponse", targetNamespace = "")
    @RequestWrapper(localName = "evaluateIteratively", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateIteratively")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:evaluateIteratively")
    @ResponseWrapper(localName = "evaluateIterativelyResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.EvaluateIterativelyResponse")
    public org.omg.spec.cdss._201105.dss.IterativeEvaluationResponse evaluateIteratively(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "iterativeEvaluationRequest", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.IterativeEvaluationRequest iterativeEvaluationRequest
    ) throws EvaluationExceptionFault, InvalidDriDataFormatExceptionFault, UnrecognizedScopedEntityExceptionFault, UnrecognizedLanguageExceptionFault, InvalidTimeZoneOffsetExceptionFault, DSSRuntimeExceptionFault, UnsupportedLanguageExceptionFault, RequiredDataNotProvidedExceptionFault;
}
