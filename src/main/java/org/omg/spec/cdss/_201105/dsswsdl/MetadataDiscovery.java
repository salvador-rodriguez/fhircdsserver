package org.omg.spec.cdss._201105.dsswsdl;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * The MetadataDiscovery Interface enables the identification of service capabilities.
 *
 * This class was generated by Apache CXF 2.4.6
 * 2015-05-14T01:22:45.860+01:00
 * Generated source version: 2.4.6
 * 
 */
@WebService(targetNamespace = "http://www.omg.org/spec/CDSS/201105/dssWsdl", name = "MetadataDiscovery")
@XmlSeeAlso({org.omg.spec.cdss._201105.dss.ObjectFactory.class})
public interface MetadataDiscovery {

    /**
     * Throws UnrecognizedScopedEntityException if the specified semantic signifier EntityIdentifier is not recognized by the service.  If specified EntityIdentifier is recognized by the service, returns a description of the semantic signifier as a SemanticSignifier object.
     */
    @WebResult(name = "semanticSignifier", targetNamespace = "")
    @RequestWrapper(localName = "describeSemanticSignifier", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeSemanticSignifier")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:describeSemanticSignifier")
    @ResponseWrapper(localName = "describeSemanticSignifierResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeSemanticSignifierResponse")
    public org.omg.spec.cdss._201105.dss.SemanticSignifier describeSemanticSignifier(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "semanticSignifierId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.EntityIdentifier semanticSignifierId
    ) throws UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault;

    /**
     * Throws UnrecognizedScopingEntityException if the specified scoping entity identifier is not recognized by the service.  If specified scoping entity identifier is recognized by the service, returns a description of the scoping entity as a ScopingEntity object.  Returned ScopingEntity object does not include any children scoping entities.
     */
    @WebResult(name = "scopingEntity", targetNamespace = "")
    @RequestWrapper(localName = "describeScopingEntity", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeScopingEntity")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:describeScopingEntity")
    @ResponseWrapper(localName = "describeScopingEntityResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeScopingEntityResponse")
    public org.omg.spec.cdss._201105.dss.ScopingEntity describeScopingEntity(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "scopingEntityId", targetNamespace = "")
        java.lang.String scopingEntityId
    ) throws UnrecognizedScopingEntityExceptionFault, DSSRuntimeExceptionFault;

    /**
     * Throws UnrecognizedScopingEntityException if the specified scoping entity identifier is not recognized by the service.  If specified scoping entity identifier is recognized by the service, returns a description of the scoping entity as a ScopingEntity object.  Returned ScopingEntity object includes any descendant scoping entities, up to and including the depth specified.
     */
    @WebResult(name = "scopingEntity", targetNamespace = "")
    @RequestWrapper(localName = "describeScopingEntityHierarchy", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeScopingEntityHierarchy")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:describeScopingEntityHierarchy")
    @ResponseWrapper(localName = "describeScopingEntityHierarchyResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeScopingEntityHierarchyResponse")
    public org.omg.spec.cdss._201105.dss.ScopingEntity describeScopingEntityHierarchy(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "scopingEntityId", targetNamespace = "")
        java.lang.String scopingEntityId,
        @WebParam(name = "maximumDescendantDepth", targetNamespace = "")
        java.math.BigInteger maximumDescendantDepth
    ) throws UnrecognizedScopingEntityExceptionFault, DSSRuntimeExceptionFault;

    /**
     * Throws UnrecognizedScopedEntityException if the specified semantic signifier EntityIdentifier is not recognized by the service.  If specified EntityIdentifier is recognized by the service, returns a description of the semantic signifier as a SemanticSignifier object.
     */
    @WebResult(name = "semanticRequirement", targetNamespace = "")
    @RequestWrapper(localName = "describeSemanticRequirement", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeSemanticRequirement")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:describeScopingEntity")
    @ResponseWrapper(localName = "describeSemanticRequirementResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeSemanticRequirementResponse")
    public org.omg.spec.cdss._201105.dss.SemanticRequirement describeSemanticRequirement(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "semanticRequirementId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.EntityIdentifier semanticRequirementId
    ) throws UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault;

    /**
     * Throws UnrecognizedScopedEntityException if the specified profile EntityIdentifier is not recognized by the service.  If specified EntityIdentifier is recognized by the service, returns a description of the profile as a ServiceProfile object.
     */
    @WebResult(name = "serviceProfile", targetNamespace = "")
    @RequestWrapper(localName = "describeProfile", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeProfile")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:describeProfile")
    @ResponseWrapper(localName = "describeProfileResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeProfileResponse")
    public org.omg.spec.cdss._201105.dss.ServiceProfile describeProfile(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "profileId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.EntityIdentifier profileId
    ) throws UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault;

    /**
     * Returns a list of all of the profiles supported by the service as a ProfilesByType object. 
     */
    @WebResult(name = "profilesByType", targetNamespace = "")
    @RequestWrapper(localName = "listProfiles", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.ListProfiles")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:listProfiles")
    @ResponseWrapper(localName = "listProfilesResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.ListProfilesResponse")
    public org.omg.spec.cdss._201105.dss.ProfilesByType listProfiles(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId
    ) throws DSSRuntimeExceptionFault;

    /**
     * Throws UnrecognizedScopedEntityException if the specified trait EntityIdentifier is not recognized by the service.  If specified EntityIdentifier is recognized by the service, returns a description of the trait used for describing knowledge modules as a Trait object.
     */
    @WebResult(name = "trait", targetNamespace = "")
    @RequestWrapper(localName = "describeTrait", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeTrait")
    @WebMethod(action = "http://www.omg.org/spec/CDSS/201105/dssWsdl:operation:describeTrait")
    @ResponseWrapper(localName = "describeTraitResponse", targetNamespace = "http://www.omg.org/spec/CDSS/201105/dss", className = "org.omg.spec.cdss._201105.dss.DescribeTraitResponse")
    public org.omg.spec.cdss._201105.dss.Trait describeTrait(
        @WebParam(name = "interactionId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.InteractionIdentifier interactionId,
        @WebParam(name = "traitId", targetNamespace = "")
        org.omg.spec.cdss._201105.dss.EntityIdentifier traitId
    ) throws UnrecognizedScopedEntityExceptionFault, DSSRuntimeExceptionFault;
}
