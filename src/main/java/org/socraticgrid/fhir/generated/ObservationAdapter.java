package org.socraticgrid.fhir.generated;

import org.socraticgrid.fhir.generated.IObservation;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.composite.ContainedDt;
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.model.dstu2.composite.RangeDt;
import ca.uhn.fhir.model.dstu2.composite.RatioDt;
import ca.uhn.fhir.model.dstu2.composite.SampledDataDt;
import ca.uhn.fhir.model.dstu2.composite.AttachmentDt;
import ca.uhn.fhir.model.primitive.TimeDt;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import java.util.Date;
import ca.uhn.fhir.model.dstu2.composite.TimingDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationValueAbsentReasonEnum;
import ca.uhn.fhir.model.dstu2.composite.BoundCodeableConceptDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationInterpretationCodesEnum;
import ca.uhn.fhir.model.primitive.InstantDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationStatusEnum;
import ca.uhn.fhir.model.primitive.BoundCodeDt;
import ca.uhn.fhir.model.dstu2.valueset.ObservationReliabilityEnum;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import java.util.List;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.resource.Specimen;
import ca.uhn.fhir.model.dstu2.resource.Encounter;
import ca.uhn.fhir.model.api.ExtensionDt;

public class ObservationAdapter implements IObservation
{

   private Observation adaptedClass = new Observation();

   public IdDt getId()
   {
      return adaptedClass.getId();
   }

   public void setId(IdDt param)
   {
      adaptedClass.setId(param);
   }

   public CodeDt getLanguage()
   {
      return adaptedClass.getLanguage();
   }

   public void setLanguage(CodeDt param)
   {
      adaptedClass.setLanguage(param);
   }

   public NarrativeDt getText()
   {
      return adaptedClass.getText();
   }

   public void setText(NarrativeDt param)
   {
      adaptedClass.setText(param);
   }

   public ContainedDt getContained()
   {
      return adaptedClass.getContained();
   }

   public void setContained(ContainedDt param)
   {
      adaptedClass.setContained(param);
   }

   public CodeableConceptDt getCode()
   {
      return adaptedClass.getCode();
   }

   public void setCode(CodeableConceptDt param)
   {
      adaptedClass.setCode(param);
   }

   public QuantityDt getValueQuantity()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.QuantityDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.QuantityDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueQuantity(QuantityDt param)
   {
      adaptedClass.setValue(param);
   }

   public CodeableConceptDt getValueCodeableConcept()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueCodeableConcept(CodeableConceptDt param)
   {
      adaptedClass.setValue(param);
   }

   public StringDt getValueStringElement()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.primitive.StringDt)
      {
         return (ca.uhn.fhir.model.primitive.StringDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public String getValueString()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.primitive.StringDt)
      {
         return ((ca.uhn.fhir.model.primitive.StringDt) adaptedClass
               .getValue()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueString(StringDt param)
   {
      adaptedClass.setValue(param);
   }

   public void setValueString(String param)
   {
      adaptedClass.setValue(new ca.uhn.fhir.model.primitive.StringDt(param));
   }

   public RangeDt getValueRange()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.RangeDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.RangeDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueRange(RangeDt param)
   {
      adaptedClass.setValue(param);
   }

   public RatioDt getValueRatio()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.RatioDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.RatioDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueRatio(RatioDt param)
   {
      adaptedClass.setValue(param);
   }

   public SampledDataDt getValueSampledData()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.SampledDataDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.SampledDataDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueSampledData(SampledDataDt param)
   {
      adaptedClass.setValue(param);
   }

   public AttachmentDt getValueAttachment()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.AttachmentDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.AttachmentDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueAttachment(AttachmentDt param)
   {
      adaptedClass.setValue(param);
   }

   public TimeDt getValueTimeElement()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.primitive.TimeDt)
      {
         return (ca.uhn.fhir.model.primitive.TimeDt) adaptedClass.getValue();
      }
      else
      {
         return null;
      }
   }

   public String getValueTime()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.primitive.TimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.TimeDt) adaptedClass
               .getValue()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueTime(TimeDt param)
   {
      adaptedClass.setValue(param);
   }

   public void setValueTime(String param)
   {
      adaptedClass.setValue(new ca.uhn.fhir.model.primitive.TimeDt(param));
   }

   public DateTimeDt getValueDateTimeElement()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public Date getValueDateTime()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getValue()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValueDateTime(DateTimeDt param)
   {
      adaptedClass.setValue(param);
   }

   public void setValueDateTime(Date param)
   {
      adaptedClass
            .setValue(new ca.uhn.fhir.model.primitive.DateTimeDt(param));
   }

   public TimingDt getValuePeriod()
   {
      if (adaptedClass.getValue() != null
            && adaptedClass.getValue() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getValue();
      }
      else
      {
         return null;
      }
   }

   public void setValuePeriod(TimingDt param)
   {
      adaptedClass.setValue(param);
   }

   public BoundCodeableConceptDt<ObservationValueAbsentReasonEnum> getDataAbsentReason()
   {
      return adaptedClass.getDataAbsentReason();
   }

   public void setDataAbsentReason(
         BoundCodeableConceptDt<ObservationValueAbsentReasonEnum> param)
   {
      adaptedClass.setDataAbsentReason(param);
   }

   public BoundCodeableConceptDt<ObservationInterpretationCodesEnum> getInterpretation()
   {
      return adaptedClass.getInterpretation();
   }

   public void setInterpretation(
         BoundCodeableConceptDt<ObservationInterpretationCodesEnum> param)
   {
      adaptedClass.setInterpretation(param);
   }

   public StringDt getCommentsElement()
   {
      return adaptedClass.getCommentsElement();
   }

   public String getComments()
   {
      return adaptedClass.getComments();
   }

   public void setComments(String param)
   {
      adaptedClass
            .setComments(new ca.uhn.fhir.model.primitive.StringDt(param));
   }

   public void setComments(StringDt param)
   {
      adaptedClass.setComments(param);
   }

   public DateTimeDt getAppliesDateTimeElement()
   {
      if (adaptedClass.getApplies() != null
            && adaptedClass.getApplies() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return (ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getApplies();
      }
      else
      {
         return null;
      }
   }

   public Date getAppliesDateTime()
   {
      if (adaptedClass.getApplies() != null
            && adaptedClass.getApplies() instanceof ca.uhn.fhir.model.primitive.DateTimeDt)
      {
         return ((ca.uhn.fhir.model.primitive.DateTimeDt) adaptedClass
               .getApplies()).getValue();
      }
      else
      {
         return null;
      }
   }

   public void setAppliesDateTime(DateTimeDt param)
   {
      adaptedClass.setApplies(param);
   }

   public void setAppliesDateTime(Date param)
   {
      adaptedClass.setApplies(new ca.uhn.fhir.model.primitive.DateTimeDt(
            param));
   }

   public TimingDt getAppliesPeriod()
   {
      if (adaptedClass.getApplies() != null
            && adaptedClass.getApplies() instanceof ca.uhn.fhir.model.dstu2.composite.TimingDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.TimingDt) adaptedClass
               .getApplies();
      }
      else
      {
         return null;
      }
   }

   public void setAppliesPeriod(TimingDt param)
   {
      adaptedClass.setApplies(param);
   }

   public InstantDt getIssuedElement()
   {
      return adaptedClass.getIssuedElement();
   }

   public Date getIssued()
   {
      return adaptedClass.getIssued();
   }

   public void setIssued(Date param)
   {
      adaptedClass
            .setIssued(new ca.uhn.fhir.model.primitive.InstantDt(param));
   }

   public void setIssued(InstantDt param)
   {
      adaptedClass.setIssued(param);
   }

   public String getStatus()
   {
      return adaptedClass.getStatus();
   }

   public void setStatus(String param)
   {
      adaptedClass.setStatus(ObservationStatusEnum.valueOf(param));
   }

   public BoundCodeDt<ObservationStatusEnum> getStatusElement()
   {
      return adaptedClass.getStatusElement();
   }

   public void setStatus(BoundCodeDt<ObservationStatusEnum> param)
   {
      adaptedClass.setStatus(param);
   }

   public String getReliability()
   {
      return adaptedClass.getReliability();
   }

   public void setReliability(String param)
   {
      adaptedClass.setReliability(ObservationReliabilityEnum.valueOf(param));
   }

   public BoundCodeDt<ObservationReliabilityEnum> getReliabilityElement()
   {
      return adaptedClass.getReliabilityElement();
   }

   public void setReliability(BoundCodeDt<ObservationReliabilityEnum> param)
   {
      adaptedClass.setReliability(param);
   }

   public CodeableConceptDt getBodySiteCodeableConcept()
   {
      if (adaptedClass.getBodySite() != null
            && adaptedClass.getBodySite() instanceof ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt) adaptedClass
               .getBodySite();
      }
      else
      {
         return null;
      }
   }

   public void setBodySiteCodeableConcept(CodeableConceptDt param)
   {
      adaptedClass.setBodySite(param);
   }

   public ResourceReferenceDt getBodySiteReference()
   {
      if (adaptedClass.getBodySite() != null
            && adaptedClass.getBodySite() instanceof ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt)
      {
         return (ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt) adaptedClass
               .getBodySite();
      }
      else
      {
         return null;
      }
   }

   public void setBodySiteReference(ResourceReferenceDt param)
   {
      adaptedClass.setBodySite(param);
   }

   public CodeableConceptDt getMethod()
   {
      return adaptedClass.getMethod();
   }

   public void setMethod(CodeableConceptDt param)
   {
      adaptedClass.setMethod(param);
   }

   public List<IdentifierDt> getIdentifier()
   {
      return adaptedClass.getIdentifier();
   }

   public void setIdentifier(List<IdentifierDt> param)
   {
      adaptedClass.setIdentifier(param);
   }

   public void addIdentifier(IdentifierDt param)
   {
      adaptedClass.getIdentifier().add(param);
   }

   public Specimen getSpecimenResource()
   {
      if (adaptedClass.getSpecimen().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Specimen)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Specimen) adaptedClass
               .getSpecimen().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setSpecimenResource(Specimen param)
   {
      adaptedClass.getSpecimen().setResource(param);
   }

   public Encounter getEncounterResource()
   {
      if (adaptedClass.getEncounter().getResource() instanceof ca.uhn.fhir.model.dstu2.resource.Encounter)
      {
         return (ca.uhn.fhir.model.dstu2.resource.Encounter) adaptedClass
               .getEncounter().getResource();
      }
      else
      {
         return null;
      }
   }

   public void setEncounterResource(Encounter param)
   {
      adaptedClass.getEncounter().setResource(param);
   }

   public Observation getAdaptee()
   {
      return adaptedClass;
   }

   public void setAdaptee(Observation param)
   {
      this.adaptedClass = param;
   }
}