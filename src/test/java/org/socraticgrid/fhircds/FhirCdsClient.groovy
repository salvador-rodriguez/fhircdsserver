package org.socraticgrid.fhircds

import org.socraticgrid.fhir.generated.Citizenship
import org.socraticgrid.fhir.generated.QICoreConditionAdapter
import org.socraticgrid.fhir.generated.QICorePatientAdapter

import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.model.api.Bundle
import ca.uhn.fhir.model.api.ResourceMetadataKeyEnum
import ca.uhn.fhir.model.dstu2.composite.CodeableConceptDt
import ca.uhn.fhir.model.dstu2.composite.CodingDt
import ca.uhn.fhir.model.dstu2.composite.HumanNameDt
import ca.uhn.fhir.model.dstu2.composite.TimingDt
import ca.uhn.fhir.model.primitive.IdDt
import ca.uhn.fhir.parser.IParser

class FhirCdsClient {

	public String generateBundleXml (FhirContext context){
		
		// condition
		QICoreConditionAdapter condition = new QICoreConditionAdapter()
		Date date = new Date()
		Calendar c = Calendar.getInstance()
		c.setTime(date)
		def dateNow = c.add(Calendar.MONTH, -3)

		condition.setOnsetDateTime(dateNow)
		
		// add profiles
		List<IdDt> profilesCondition = new ArrayList<IdDt>();
		profilesCondition.add(new IdDt("http://hl7.org/fhir/StructureDefinition/condition-qicore-qicore-condition"));
		//profiles.add(new IdDt("http://foo/Profile2"));
		ResourceMetadataKeyEnum.PROFILES.put(condition.getAdaptee(), profilesCondition);

		CodeableConceptDt codeableConceptDt = new CodeableConceptDt()
		CodingDt codingDt = new CodingDt()
		codingDt.setCode("73211009")
		codingDt.setDisplay("Diabetes mellitus (disorder)")
		codingDt.setSystem("SNOMED-CT")
		codeableConceptDt.getCoding().add(codingDt)

		condition.setCode(codeableConceptDt)

		// patient
		QICorePatientAdapter corePt = new QICorePatientAdapter()
		HumanNameDt humanName = new HumanNameDt()
		humanName.addFamily("Smith").addGiven("John");
		corePt.addName(humanName)
		
		// add profiles
		List<IdDt> profiles = new ArrayList<IdDt>();
		profiles.add(new IdDt("http://hl7.org/fhir/StructureDefinition/patient-qicore-qicore-patient"));
		//profiles.add(new IdDt("http://foo/Profile2"));
		ResourceMetadataKeyEnum.PROFILES.put(corePt.getAdaptee(), profiles);

		// add religion
		CodeableConceptDt religion = new CodeableConceptDt()
		CodingDt religionCode = new CodingDt("http://thedevine", "Spiritual3")

		// add citizenship
		List<Citizenship> citizenships = new ArrayList<Citizenship>()
		Citizenship citizenship = new Citizenship()
		CodeableConceptDt codeableConcept = new CodeableConceptDt()
		CodingDt coding = new CodingDt("http://snomedct", "234")
		TimingDt timing = new TimingDt()
		timing.addEvent(new Date())

		religion.getCoding().add(religionCode)

		corePt.setReligion(religion)
		corePt.setActive(true)
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(0);
		cal.set(1975, 1, 1, 0, 0, 0);
		Date dob = cal.getTime(); // get back a Date object
		corePt.setBirthDate(dob)

		coding.setDisplay("My first code")
		codeableConcept.getCoding().add(coding)
		citizenship.setCode(codeableConcept)

		citizenship.setPeriod(timing)
		citizenships.add(citizenship)
		
		corePt.setCitizenship(citizenships)

		// add resources to bundle
		IParser parser = context.newXmlParser()
		String theServerBase = ""  // assume this is okay here; not sure
		Bundle bundle = new Bundle()
		bundle.addResource(corePt.getAdaptee(), context, theServerBase)
		bundle.addResource(condition.getAdaptee(), context, theServerBase)
		
		String stringBundle = context.newXmlParser().setPrettyPrint(true).encodeBundleToString(bundle)

		return stringBundle
	}
	
}
