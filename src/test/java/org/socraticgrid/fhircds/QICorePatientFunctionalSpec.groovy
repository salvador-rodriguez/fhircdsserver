package org.socraticgrid.fhircds

import org.opencds.fhircds.EvaluateImpl
import org.opencds.fhircds.Kie
import org.opencds.fhircds.RuleEngine

import spock.lang.Specification
import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.model.api.*

class QICorePatientFunctionalSpec extends Specification {

	private static final samplesPath = "src/test/resources/samples"
	FhirCdsClient fhirCdsClient = new FhirCdsClient()
	FhirContext context = FhirContext.forDstu2()
	String bundle
	
	def "generate bundle XML"(){
		when:
		bundle = fhirCdsClient.generateBundleXml(context)
		
		then:
		bundle != null
		println bundle
		new java.io.FileWriter("${samplesPath}/bundle.xml").withWriter{it << bundle}
		
	}
	
	def "insert resources into kie"(){
		when:
		// Create Bundle
		def inputFile = new File("${samplesPath}/bundle.xml").text
		
		String group_id = "org.opencds"
		//String artifact_id = "easycds-kb"
	    //String artifact_version = "0.3.0"
		String artifact_id = "fhir-kb"
		String artifact_version = "1.0.0"
		
		Kie kie = new Kie()
		kie.Evaluation(group_id, artifact_id, artifact_version, inputFile)
		
		then:
		true
	}
	
	
	def "insert facts into RuleEngine"(){
		when:
		// Create Bundle
		def inputFile = new File("${samplesPath}/bundle.xml").text
		RuleEngine ruleEngine = new RuleEngine()
		ruleEngine.insertFacts(inputFile)
		
		then:
		true
	}
	
/*	def "test EvaluationImpl"(){
		when:
		EvaluateImpl evaluationImpl = new EvaluateImpl ();
		evaluationImpl.evaluateAtSpecifiedTime()
		
		then:
	}*/

}