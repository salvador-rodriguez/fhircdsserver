package org.opencds.fhircds;

import org.kie.api.KieServices
import org.kie.api.runtime.KieContainer
import org.kie.api.runtime.KieSession
import org.socraticgrid.fhir.generated.AdapterFactory
import org.socraticgrid.fhir.generated.Citizenship
import org.socraticgrid.fhir.generated.IQICorePatient
import org.socraticgrid.fhir.generated.QICorePatientAdapter

import spock.lang.Specification
import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.model.dstu2.resource.Patient
import ca.uhn.fhir.parser.IParser

class KnowledgeBaseFunctionalSpec extends Specification {

	private static final samplesPath = "src/test/resources/samples"

	KieServices ks
	KieContainer kContainer
	KieSession kSession

	def setup () {
		ks = KieServices.Factory.get()
		kContainer = ks.getKieClasspathContainer()
	}

	def "run simple process"(){
		setup:
		kSession = kContainer.newKieSession("ksession-process")

		when:
		// start a new process instance
		def result = kSession.startProcess("com.sample.bpmn.hello");

		then:
		result == 1

	}

	def "evaluate patient.xml" () {

		setup:
		kSession = kContainer.newKieSession("ksession-rules")
		def inputPatientFile = new File("${samplesPath}/patient.xml").text
		StringReader reader = new StringReader(inputPatientFile)

		FhirContext ctx = new FhirContext()
		IParser parser = ctx.newXmlParser()
		Patient patient = parser.parseResource(Patient.class, reader)
		
//		Citizenship citizenship = parser.parseResource(Citizenship.class, reader)
//		def codeValue = citizenship.getCode().getCodingFirstRep().getCode()
//		println codeValue

		AdapterFactory factory = new AdapterFactory()
		def patientAdapter = factory.adapt(patient)
		
		QICorePatientAdapter qiCorePatient = new QICorePatientAdapter()
		qiCorePatient.setAdaptee(patient)

		def list = qiCorePatient.getCitizenship()
		def citiList = list.get(0).getCode()
		
		def code = citiList.getCodingFirstRep().getCode()
		println code
		
		when:
		kSession.insert(qiCorePatient)
		def result = kSession.fireAllRules()

		then:
		result == 1
	}
}
