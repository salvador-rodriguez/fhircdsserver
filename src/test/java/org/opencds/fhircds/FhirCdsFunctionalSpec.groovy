package org.opencds.fhircds

import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar

import org.omg.spec.cdss._201105.dss.DataRequirementItemData
import org.omg.spec.cdss._201105.dss.EntityIdentifier
import org.omg.spec.cdss._201105.dss.EvaluationRequest
import org.omg.spec.cdss._201105.dss.EvaluationResponse
import org.omg.spec.cdss._201105.dss.InteractionIdentifier
import org.omg.spec.cdss._201105.dss.ItemIdentifier
import org.omg.spec.cdss._201105.dss.KMEvaluationRequest
import org.omg.spec.cdss._201105.dss.SemanticPayload

import spock.lang.Specification
import ca.uhn.fhir.context.FhirContext
import ca.uhn.fhir.model.api.*

class FhirCdsFunctionalSpec extends Specification {

	private static final samplesPath = "src/test/resources/samples"
	FhirCdsClient fhirCdsClient = new FhirCdsClient()
	FhirContext context = FhirContext.forDstu2()
	String bundle
	
	def "generate bundle XML"(){
		when:
		bundle = fhirCdsClient.generateBundleXml(context)
		
		then:
		bundle != null
		println bundle
		new java.io.FileWriter("${samplesPath}/bundle.xml").withWriter{it << bundle}
		
	}
	
	def "evaluate fhir bundle against maven kb"(){
		when:

		def inputFhirBundle = new File("${samplesPath}/bundle.xml").text
		
		String group_id = "org.opencds"
		String artifact_id = "fhir-kb"
		String artifact_version = "1.0.0"
		
		Kie kie = new Kie()
		
		Bundle bundle = kie.Evaluation(group_id, artifact_id, artifact_version, inputFhirBundle)
		String stringBundle = context.newXmlParser().setPrettyPrint(true).encodeBundleToString(bundle)
		
		then:
		println stringBundle
	}
	
	
	def "evaluate fhir bundle against kb contained in current project"(){
		when:

		def inputFhirBundle = new File("${samplesPath}/bundle.xml").text
		
		Kie kie = new Kie()	
		
		Bundle bundle = kie.Evaluation(inputFhirBundle)
		String stringBundle = context.newXmlParser().setPrettyPrint(true).encodeBundleToString(bundle)
		
		then:
		println stringBundle
	}
	
	def "EvaluationImpl test"(){
		when:
		
		EvaluateImpl evaluationImpl = new EvaluateImpl ();
		InteractionIdentifier interactionId = new InteractionIdentifier();
		interactionId.setInteractionId("123456");
		interactionId.setScopingEntityId("edu.utah");
		
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
		XMLGregorianCalendar specifiedTime =
			datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);

		interactionId.setSubmissionTime(specifiedTime);
		
		EvaluationRequest evaluationRequest = new EvaluationRequest();
		evaluationRequest.setClientLanguage("?");
		evaluationRequest.setClientTimeZoneOffset("?");
		
		List<KMEvaluationRequest> listKmEvaluationRequest = evaluationRequest.getKmEvaluationRequest();
		
		KMEvaluationRequest kmEvaluationRequest = new KMEvaluationRequest();

		EntityIdentifier kmId = new EntityIdentifier();
		kmId.setScopingEntityId("org.opencds");
		kmId.setBusinessId("demo1");
		kmId.setVersion("0.0.1");
		
		kmEvaluationRequest.setKmId(kmId );
		listKmEvaluationRequest.add(kmEvaluationRequest);
		
		List<DataRequirementItemData> listDataRequirementItemData = evaluationRequest.getDataRequirementItemData();

		DataRequirementItemData dataRequirementItemData = new DataRequirementItemData();
		
		ItemIdentifier driId = new ItemIdentifier();
		driId.setItemId("fhir_payload");
		
		EntityIdentifier containingEntityid = new EntityIdentifier();
		
		// these are used as maven GAV
		containingEntityid.setScopingEntityId("org.opencds");
		containingEntityid.setBusinessId("fhir-kb");
		containingEntityid.setVersion("1.0.0");
		
		driId.setContainingEntityId(containingEntityid );
		dataRequirementItemData.setDriId(driId );
		
		// payload
		SemanticPayload semanticPayload = new SemanticPayload();
		
		EntityIdentifier informationModelISSId = new EntityIdentifier();
		informationModelISSId.setScopingEntityId("org.opencds.fhir");
		informationModelISSId.setBusinessId("FHIR");
		informationModelISSId.setVersion("1.0.0");
		
		semanticPayload.setInformationModelSSId(informationModelISSId );
		
		List<byte[]> listBase64EncodedPayload = semanticPayload.getBase64EncodedPayload();
		
		// build input from bundle.xml
		String inputFhirBundle = new File("${samplesPath}/bundle.xml").text
		println inputFhirBundle
		
		byte[] base64Payload = inputFhirBundle.bytes.encodeBase64().toString().bytes
		println base64Payload
	
		listBase64EncodedPayload.add(base64Payload);
		
		dataRequirementItemData.setData(semanticPayload );
		listDataRequirementItemData.add(dataRequirementItemData);
					
		EvaluationResponse evaluationResponse = evaluationImpl.evaluateAtSpecifiedTime(interactionId,specifiedTime,evaluationRequest);
		
		then:
		println "true"
	}

}